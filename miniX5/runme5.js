let x = 0;
let y = 0;
let minWeight = 2;


function setup(){
//Vi skal lave et canvas, der fylder hele skærmen
createCanvas(windowWidth, windowHeight);
//Vi skal tegne skrå linjer, i to retninger
background(255,210,100);
frameRate(30);
}


function draw(){
stroke(random(255),random(190), 0);
strokeWeight(minWeight);
minWeight += 0.007;


const minRandom = random(1);

if(minRandom < 0.5){
    //tegn en slags streg
   
    line(x, y, x+40, y+40);
} 
else {
    //teng den anden streg
   
    line(x, y+40, x+40, y);
}

x += 40;
if(x > width){
    y = y+40;
    x = 0;
}

}
function windowResized(){
    resizeCanvas(windowWidth,windowHeight);
}