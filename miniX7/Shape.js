
class Shape{
    constructor(){
        this.posX = random(0, width);
        this.posY = random(0, height);
        this.speed = random(0.5, 3);
        this.size = random(10, 190);
        frameRate(15);
    }

    show(){
        rectMode(CENTER);
        noStroke();
        //Rektanglerne, der bevæger sig henover skærmen
        fill(random(100, 255), random(100, 255), random(100, 255));
        rect(this.posX, this.posY, this.size, this.size); 
    }

    move(){
        this.posX += this.speed
        if(this.posX > width){
            this.posX = 0;
        }
    }
}