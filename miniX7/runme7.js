let antal = 18;
let shape = [];

function preload(){
    img = loadImage("gradient.webp");
    ambient = loadSound("ambientSound.mp3");
}

function setup(){
   
    createCanvas(600, 700);
    background(img);
    
    for (let i = 0; i < antal; i++){
        shape.push(new Shape());
      }
      spawnShape();

}

function draw(){
    background(img);
    spawnShape();
    ambient.play();
   


}

function spawnShape() {
    for (let i = 0; i < shape.length; i++) {
        shape[i].move();
        shape[i].show();
    }
}