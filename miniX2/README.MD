# miniX2
**Af Isak Hegestand-Sverdlin**

Jamen jamen, så er vi her igen, en uge efter den første _miniX_. Jeg synes det bliver mere og mere interessant at lave disse opgaver. Sidste uge, fandt jeg også opgaven sjov. Men jeg blev også frustreret, over mine egen begrænsninger, indenfor kodning. Det irriterede mig at jeg ikke kunne lave opgaven, så komplekst som jeg egentlig gerne ville. Det er egentlig meget i samme tråd, som de følelser jeg generelt har, omkring en kreativ opgave. 

## Information om min kreative baggrund 

I min skolegang på Rudolf Steiner, havde vi en masse af disse kreative fag. Et af disse var plasticering, som bare er et flot ord, for at lave skulpturer af ler. I dette fag, var jeg også skuffet over, hvor ringe mine evner var. Jeg ville gerne bare springe ud i at lave den nye Davids skulptur. Men Michelangelo havde nok også en læringsperiode, hvor han lige skulle blive super dygtig. Han kunne ikke bare lave en af de flotteste skulpturer nogensinde, uden lidt træning. Ikke fordi de ting jeg lavede i faget, endte med at blive så flotte. Langt fra. Jeg mener bare at jeg bliver så inspireret, af de flotte værker jeg ser, så jeg forventer af mig selv at kunne det samme. Men det er nok en smule urealistisk, for at sige det mildt. Og på samme måde har jeg det med kodning. Jeg vil gerne bare lave ”den perfekte kode”, til at starte med. Men her skal jeg nok lige lære at være realistisk, i mine egne forventninger til mig selv. 

## Processen med _miniX2_ 

Min proces med _miniX2_, var fra starten af mere gnidningsfri, end den fra ugen før. Jeg satte mig selv det mål, at jeg gerne ville lave to ansigter, i en simpel grafisk stil. Dette gjorde jeg, fordi jeg mente det var noget jeg kunne lave, med de evner jeg har på nuværende tidspunkt. Med en målsætning som denne, var det mere sandsynligt at jeg ville få den her ”succes-oplevelse”, uden alle de frustrationer jeg oplevede sidst.

Min kode er egentlig ret simpel, men jeg synes den formår at lave det billede, som jeg havde i tankerne. I min ”draw” sekvens, har jeg sat alle de geometriske former ind, der tilsammen danner det samlede ansigt. Hver form er navngivet, så det er en smule nemmere at finde rundt i. Til at begynde med, var det hele sat op med `width/2` og `height/2` som x- og y-koordinater. Men det synes jeg blev for upræcist, fordi jeg flere gange ændrede på målene af mit canvas. Derfor var det nemmere for mig at låse formerne fast, med præcise koordinater. 

De detaljer på ansigtet, som jeg gerne ville fremhæve, har desuden et lag ”skygge”, som bare er samme form, i en mørkere tone. De er samtidig en smule mindre, for at give den ønskede effekt. 

## Spas med kode 

Af sjove _syntaxer_ jeg fandt, kan nævnes måden at lave en `rektangel()` blød i kanterne på. Dette gøres ved at tilføje fire ekstra tal, i rektanglens argument. Disse fire tal, er hvert hjørnes radius. Denne kan så være lige så rund eller skarp, som man ønsker i den givne situation. 

Øjnene valgte jeg at lave asymmetriske, og med detaljer, der slet ikke er anatomisk korrekte. Jeg valgte denne løsning, for at være med til at skubbe ansigterne væk fra det menneskelige. Mine to ansigter, skal ikke ligne et menneskes ansigt. Tværtimod. Derfor fik de også nogle grundfarver, som slet ikke er at finde i den virkelige verden. Jeg tænkte at det ville være med til at skubbe debatten væk, fra en emoji, som det er meningen man skal kunne se sig selv i. Og mere over i noget abstrakt, som kan nydes, uden bagtanke. Ansigterne er ikke lavet for at ligne en mand eller en kvinde. De kan ses som værende to rumvæsner, eller hvad end beskueren har lyst til. Det vil jeg lade være op til individuel fortolkning.

![](/miniX2/Screenshot_2023-02-17_at_13.21.34.png)

## Emoji 

Denne debat om emoji, og alle de politiske dagsordner, der bliver puttet ind over, er ikke noget jeg til daglig tænker særlig meget over. Når jeg sender en emoji, der griner, tænker jeg ikke den skal ligne mig selv. Så havde jeg nok bare sendt et billede, hvor jeg griner. Jeg tænker det mere, som en abstrakt karikatur af en følelse. Hvis da en følelse overhovedet kan karikeres. Dog kan jeg sagtens forstå at nogen mennesker kan blive krænket eller stødt, over den måde en given emoji ser ud på. Eller hvordan den bruges. Og det er helt klart også en debat, som vi skal tage. Men jeg tænker det skal være på baggrund af nogle af de større diskriminationer, der finder sted verden over…


[Link](https://isakisensei.gitlab.io/aestetiskprogrammeing/miniX2/index.html) til at køre koden. **Klik på skærmen, for at få baggrunden til at skifte farve** 

[Link](https://gitlab.com/IsakiSensei/aestetiskprogrammeing/-/blob/main/miniX2/runme2.js) til source code
