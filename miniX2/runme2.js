

function setup() {
    // put setup code here
    createCanvas(1500, 1000);
    background(159,250,240);
    

    console.log(width, height);
  }
  


  function draw() {
    // put drawing code here
    
    
    
    //EMOJI 1
    
    //hoved
    noStroke();
    fill(255,105,105);
    ellipse(1000,height/2, 500);

    
    //højre øre
    noStroke();
    fill(255,105,105);
    ellipse(1280, height/2, 80, 100);
    
    //højre øre skygge
    noStroke();
    fill(158,32,32);
    ellipse(1275,500,50,70);


    //højre øje
    noStroke();
    fill(255,20,147);
    ellipse(1150, height/2,120, 100);

    //højre øje skyge
    noStroke();
    fill(255,161,231);
    ellipse(1142,500,100,80);

    //iris højre øje
    noStroke();
    fill(10,10,10);
    rect(1160,500,60,40);

    //venstre øje
    noStroke();
    fill(93,14,182);
    rect(850, height/2,100,100);

    //venstre øje skygge
    rectMode(CORNER);
    noStroke();
    fill(191,128,233);
    rect(800,450,80,80,0,0,20,0);

    //iris venstre øje
    noStroke();
    fill(246,238,205);
    ellipse(855,500,65,90);
    
    //torso
    noStroke();
    fill(255,105,105);
    rectMode(CENTER);
    rect(1000,850,500,200);

    //torse skygge
    noStroke();
    fill(158,32,32);
    rect(1000,800,500,100);


    //mund
    push();
    noStroke();
    fill(144,56,56);
    arc(1005, 600, 150, 150, 0,PI, CHORD);
    pop();

    
    //mund skygge
    noStroke();
    fill(240,128,128);
    arc(1005, 600, 150, 100, 0,PI, CHORD);


   //EMOJI 2
   
    //hoved
    noStroke();
    fill(125,239,231);
    ellipse(300,height/2, 500);

    
    //højre øre
    noStroke();
    fill(125,239,231);
    ellipse(580, height/2, 80, 100);
    
    //højre øre skygge
    noStroke();
    fill(64,145,140);
    ellipse(575,500,50,70);


    //højre øje
    noStroke();
    fill(255,20,147);
    ellipse(450, height/2,120, 100);

    //højre øje skyge
    noStroke();
    fill(255,161,231);
    ellipse(442,500,100,80);

    //iris højre øje
    noStroke();
    fill(10,10,10);
    rect(460,500,60,40);

    //venstre øje
    noStroke();
    fill(93,14,182);
    rect(150, height/2,100,100);

    //venstre øje skygge
    rectMode(CORNER);
    noStroke();
    fill(191,128,233);
    rect(100,450,80,80,0,0,20,0);

    //iris venstre øje
    noStroke();
    fill(246,238,205);
    ellipse(155,500,65,90);
    
    //torso
    noStroke();
    fill(125,239,231);
    rectMode(CENTER);
    rect(300,850,500,200);

    //torse skygge
    noStroke();
    fill(64,145,140);
    rect(300,800,500,100);


    //mund
    push();
    noStroke();
    fill(144,56,56);
    arc(305, 600, 150, 150, 0,PI, CHORD);
    pop();

    
    //mund skygge
    noStroke();
    fill(240,128,128);
    arc(305, 600, 150, 100, 0,PI, CHORD);

    
   

    
}
function mousePressed(){ 
  background(random(1,255),random(1,255),random(1,255));
  
    

}
  

    
    
    
    
  
