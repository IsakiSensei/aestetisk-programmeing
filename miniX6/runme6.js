let koran = [];
let rasmus;
let antal = 6;
let goal;

//Her loader jeg mit baggrundsbillede
function preload(){
    img = loadImage("moske.jpg");
}




function setup(){

    createCanvas(600, 800);
    background (img);
    //Mit for-loop, der for hver frame, laver en ny koran
    for (let i = 0; i < antal; i++){
      koran.push(new Koran());
    }
    rasmus = new Rasmus();
    goal = new Goal();
    
    
}

function draw(){
   
    background (img);
    rasmus.show();
    collision();
    spawnKoran();
    //Teksten, der beskriver hvordan spillet styres
    fill(255, 221, 46);
        textSize(35);
        textFont("SignPainter");
        text("BRUG PILETASTER", width/2+70, height/2+280);
    

    
}
//Min funktion, der viser og bevæger de koraner, der bliver lavet
function spawnKoran() {
    for (let i = 0; i < koran.length; i++) {
        koran[i].move();
        koran[i].show();
    }
}

//Key-pressed, der lader min karakter bevæge sig
function keyPressed(){
    if (keyCode === UP_ARROW){
        rasmus.moveUp();
    } else if (keyCode === DOWN_ARROW){
        rasmus.moveDown();
    } else if (keyCode === RIGHT_ARROW){
        rasmus.moveRight();
    } else if (keyCode === LEFT_ARROW){
        rasmus.moveLeft();
    }
}

//Denne funktion tjekker, om spilleren kolliderer med koranerne, og i så fald, stopper spillet
function collision(){
    let distance;
    for (let i = 0; i < koran.length; i++){
    distance = dist(rasmus.posX, rasmus.posY, koran[i].posX, koran[i].posY);
    if(distance < rasmus.size/2+10 + koran[i].size/2+10){
        noLoop();
        fill(255);
        textSize(40);
        textFont("SignPainter");
        text("DU VAR IKKE HØJREORIENTERET NOK..", width/2-240, height/2+300);
       
    }

    }
    //Dette er den funktion, der regristrerer om spilleren når toppen af banen. Den virker ikke helt optimalt
    let distance1;
    for (let i = 0; i < koran.length; i++){
    distance1 = dist(rasmus.posX, rasmus.posY, goal.posX, goal.posY);
    if(distance1 < rasmus.size/2 + goal.size/4){
        noLoop();
        fill(255);
        textSize(50);
        textFont("SignPainter");
        text("V FOR SEJR", width/2-100, height/2+300);
    }

    }

}



  
