class Rasmus{
    constructor(){
        this.posX = width/2;
        this.posY = height-50;
        this.speed = 20;
        this.size = 80;
    }

    show(){
        noStroke();
        rectMode(CENTER);
        
        //Hat på figuren
        push();
        fill(0);
        rect(this.posX+10, this.posY-50, this.size-20);
        ellipse(this.posX-30, this.posY-51, this.size-20, this.size-55);
        pop();

        //Selve Rasmus' ansigt
        fill(255, 200, 160);
        rect(this.posX, this.posY, this.size);

        //Ørene, både det venstre og højre
        fill(255, 200, 160);
        ellipse(this.posX-40, this.posY-10, this.size-55);
        ellipse(this.posX+40, this.posY-10, this.size-55);

        //Skyggerne, der viser selve "lydhullet"
        fill(205, 135, 82);
        ellipse(this.posX-40, this.posY-10, this.size-65, this.size-65);
        ellipse(this.posX+40, this.posY-10, this.size-65, this.size-65);

        //Øjnene, både venstre og højre
        fill(90, 150, 210);
        ellipse(this.posX-20, this.posY-10, this.size-65, this.size-60);
        ellipse(this.posX+20, this.posY-10, this.size-65, this.size-60);
        

        //Munden, med det flotte smil.
        fill(205, 135, 82);
        arc(this.posX, this.posY+10, this.size-25, this.size-25, 0,  PI, );

        //Stram Kurs logo, der sidder på kasketten
        fill(255, 0, 0);
        rect(this.posX, this.posY-60, this.size-60, this.size-70);
        fill(0, 0, 255);
        rect(this.posX+15, this.posY-60, this.size-95, this.size-70);

        //Iris i øjnene
        fill(0);
        ellipse(this.posX-20, this.posY-10, this.size-75, this.size-70);
        ellipse(this.posX+20, this.posY-10, this.size-75, this.size-70);
       
        
    }

    moveUp(){
        this.posY -= this.speed; 
    }

    moveDown(){
        this.posY += this.speed; 
    }

    moveRight(){
        this.posX += this.speed;
    }

    moveLeft(){
        this.posX -= this.speed;
    }

}