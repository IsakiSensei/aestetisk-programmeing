class Koran{
    constructor(){
        this.posX = random(0, width);
        this.posY = random(0, height-150);
        this.speed = random(0.5, 1,2);
        this.size = 80;
    }

    show(){
        rectMode(CENTER);
        noStroke();
        //Koranen
        fill(50, 118, 40);
        rect(this.posX, this.posY, this.size, this.size+19);

        fill(255, 221, 46);
        //Midtermønsteret, på bogen
        rect(this.posX, this.posY, this.size-30);
        rect(this.posX, this.posY-25, this.size-60);
        rect(this.posX, this.posY+25, this.size-60);

        //Hjørnerne på bogen
        rect(this.posX+33, this.posY-43, this.size-65);
        rect(this.posX-33, this.posY+43, this.size-65);
        rect(this.posX+33, this.posY+43, this.size-65);
        rect(this.posX-33, this.posY-43, this.size-65);





      
    }

    move(){
        this.posX += this.speed
        if(this.posX > width){
            this.posX = 0;
        }
    }
}