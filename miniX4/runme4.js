let vinkel=0
let knap;

let cnv;
let dia;
let dia2;
let dia3;
let dia4;



function setup(){
    
    angleMode(DEGREES);
    background(173,212,218,43);
    cnv = createCanvas(windowWidth, windowHeight);
  cnv.mousePressed(); 
  dia = 400;
  dia2 = 300;
  dia3 = 200;
  dia4 = 100;

 
}

function draw(){
    background(173,212,218,30);
    knap = createButton("PRESS TO CAPTURE THE SUN")
    knap.position(width/2-150,height/10);
    knap.size(300,100);
    knap.style("background-color","#DE8383");
    knap.style("color","#F7FF00");

   


    //Disse elementer er alle "ringene" i solen. Her bruges "dia", til at få diameteren til at ændre sig.
    push();
    translate(width/2, height/2);
    rotate(vinkel);
    noStroke();
    fill(255,190,100);
    ellipse (5, 5, dia);
    fill(255,210,110);
    ellipse(5, 5, dia2);
    fill(255,230,120);
    ellipse(5, 5, dia3);
    fill(255,255,130);
    ellipse(5, 5, dia4);
    vinkel+=10;
    pop();

    //Dette er skyen til venstre
    push();
    noStroke();
    ellipse(width/2-550,height/2-100,400,250);
    ellipse(width/2-500,height/2-200,200);
    ellipse(width/2-600,height/2-300, 100);
    pop();

    //Dette er skyen nederst til højre
    push();
    noStroke();
    ellipse(width/2+450, height/2+200, 300,250);
    ellipse(width/2+500, height/2+100, 200);
    pop();

    //Dette er skyen øverst til højre
    push();
    noStroke();
    ellipse(width/2+550,height/2-200,300);
    pop();
   
    
}

function windowResized(){
    resizeCanvas(windowWidth,windowHeight);
}


//Denne funktion registrerer klik, lige meget hvornår de sker
function mousePressed() {
  dia = dia - 40;
  dia2 = dia2 -30;
  dia3 = dia3 -20;
  dia4 = dia4 -10;
  
}

