console.log("danceman");

class Player{

    constructor(){
        rectMode(CENTER);
        imageMode(CENTER);
        this.posX = width/2;
        this.posY = height/2;
        this.speed = 5;
        this.sizeX = 50;
        this.sizeY = 50;
    }

    show(){
        noFill();
        image(zuckerberg,this.posX,this.posY, this.sizeX, this.sizeY);
       
    }

    move(){
        this.posX = this.posX + random(this.speed);
        this.posX = this.posX - random(this.speed);
        this.posY = this.posY + random(this.speed);
        this.posY = this.posY - random(this.speed);


    }
}
