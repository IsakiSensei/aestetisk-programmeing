//Formerne, der roterer på canvasset
let vinkel = 0;
let minusVinkel = 30;
let andenVinkel = 10;
let tredjeVinkel = 40;
let fjerdeVinkel = 60;
let femteVinkel = 80;
let sjeteVinkel = 100;

//Formernes 'trails', der følger i sporene
let syvendeVinkel = 100;
let ottendeVinkel = 50;
let niendeVinkel = 120;
let tiendeVinkel = 100;
let ellevteVinekl = 60;
let tolvteVinkel = 50;
let trettendeVinkel = 20;





function setup(){
    createCanvas(windowWidth,windowHeight);
    background(242,223,184);
    angleMode(DEGREES);
    rectMode(CENTER);
    frameRate(60);
    
}




function draw(){

    background(242,223,184,60);
    
    


   //1 iris
   push();
   translate(width/2,height/2);
   noStroke();
   rotate(fjerdeVinkel);
   fill(255);
   ellipse(8,25,350,70);
   fjerdeVinkel-=10.7;
   pop();

   //iris trail
    push();
    translate(width/2,height/2);
    noStroke();
    rotate(syvendeVinkel);
    fill(255,40);
    ellipse(8,25,350,70);
    syvendeVinkel-=10.7;
    pop();


    //2 Næstinderste ellipse 
    push();
    translate(width/2,height/2);
    noStroke();
    rotate(ottendeVinkel);
    fill(75,30,50,);
    ellipse(10,20,190,110);
    ottendeVinkel+=10.5;
    pop();


    //Næstinderste ellipse trail
   push();
   translate(width/2,height/2);
   noStroke();
   rotate(andenVinkel);
   fill(75,10,50,40);
   ellipse(10,20,190,110);
   andenVinkel+=10.5;
   pop();


   //3 Trekant
   push();
   translate(width/2,height/2);
   noStroke();
   fill(50,156,100); 
   rotate(femteVinkel);
   triangle(10, 225, 40, 60, 300, 115);
   femteVinkel-=10.7;
   pop();


   //Trekant trail
   push();
   translate(width/2,height/2);
   noStroke();
   fill(50,156,100,40); 
   rotate(tiendeVinkel);
   triangle(10, 225, 40, 60, 300, 115);
   tiendeVinkel-=10.7;
   pop();
    
    //4 Inderste firkant 
    push();
    translate(width/2,height/2);
    noStroke();
    fill(250,56,150); 
    rotate(vinkel);
    rect(130,130,250,50);
    vinkel-=1.1;
    pop();

    //Inderste firkant trail
    push();
    translate(width/2,height/2);
    noStroke();
    fill(250,56,150,40); 
    rotate(trettendeVinkel);
    rect(130,130,250,50);
    trettendeVinkel-=1.1;
    pop();

    //5 Yderste ellipse
    push();
    translate(width/2,height/2);
    noStroke();
    rotate(minusVinkel);
    fill(10,50,190,40);
    ellipse(130,230,240,45);
    minusVinkel+=2.8;
    pop();


     //Yderste ellipse trail
     push();
     translate(width/2,height/2);
     noStroke();
     rotate(tolvteVinkel);
     fill(10,50,190,);
     ellipse(130,230,240,45);
     tolvteVinkel+=2.8;
     pop();
     
     
    //6 Næstyderste rect
    push();
    translate(width/2,height/2);
    noStroke();
    fill(250,156,150); 
    rotate(tredjeVinkel);
    rect(230,230,300,190);
    tredjeVinkel-=2.7;
    pop();

     //Næstyderste rect trail
     push();
     translate(width/2,height/2);
     noStroke();
     fill(250,156,150,40); 
     rotate(ellevteVinekl);
     rect(230,230,300,190);
     ellevteVinekl-=2.7;
     pop();



  //7 Yderste rect
  push();
  translate(width/2,height/2);
  noStroke();
  fill(150,130,150); 
  rotate(niendeVinkel);
  rect(380,280,550,150);
  niendeVinkel+=2.3;
  pop();

  //Yderste rect trail
  push();
  translate(width/2,height/2);
  noStroke();
  fill(150,130,150,40); 
  rotate(sjeteVinkel);
  rect(380,280,550,150);
  sjeteVinkel+=2.3;
  pop();

  

}
function windowResized(){
  resizeCanvas(windowWidth,windowHeight);
}

